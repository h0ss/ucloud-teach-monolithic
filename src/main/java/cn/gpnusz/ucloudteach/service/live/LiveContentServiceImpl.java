package cn.gpnusz.ucloudteach.service.live;

import cn.gpnusz.ucloudteach.common.CommonResp;
import cn.gpnusz.ucloudteach.common.PageResp;
import cn.gpnusz.ucloudteach.entity.*;
import cn.gpnusz.ucloudteach.mapper.LiveContentMapper;
import cn.gpnusz.ucloudteach.util.PageInfoUtil;
import cn.gpnusz.ucloudteach.util.SnowFlake;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;

/**
 * @author h0ss
 * @description 直播课程内容业务层
 * @date 2022/4/4 20:49
 */
@Service
public class LiveContentServiceImpl {
    @Resource
    private LiveContentMapper liveContentMapper;

    @Resource
    private LiveTokenServiceImpl liveTokenService;

    @Resource
    private SnowFlake snowFlake;

    private static final Logger LOG = LoggerFactory.getLogger(LiveContentServiceImpl.class);

    private static final Integer MAX_SIZE_PER_PAGE = 100;

    /**
     * 根据传入条件查询直播内容
     *
     * @param lcq : 查询实体
     * @return : cn.gpnusz.ucloudteach.common.PageResp<cn.gpnusz.ucloudteach.entity.LiveContent>
     * @author h0ss
     */
    public PageResp<LiveContent> getAllByCondition(LiveContentQueryReq lcq) {
        LiveContentExample liveContentExample = new LiveContentExample();
        LiveContentExample.Criteria criteria = liveContentExample.createCriteria();
        liveContentExample.setOrderByClause("sort");
        if (!ObjectUtils.isEmpty(lcq.getId())) {
            criteria.andIdEqualTo(lcq.getId());
        }
        if (!ObjectUtils.isEmpty(lcq.getSort())) {
            criteria.andSortEqualTo(lcq.getSort());
        }
        if (!ObjectUtils.isEmpty(lcq.getName())) {
            criteria.andNameLike("%" + lcq.getName() + "%");
        }
        if (!ObjectUtils.isEmpty(lcq.getLiveId())) {
            criteria.andLiveIdEqualTo(lcq.getLiveId());
        }
        if (lcq.getPage() != null && lcq.getSize() != null) {
            if (lcq.getSize() < MAX_SIZE_PER_PAGE) {
                PageHelper.startPage(lcq.getPage(), lcq.getSize());
            } else {
                PageHelper.startPage(lcq.getPage(), MAX_SIZE_PER_PAGE);
            }
        } else {
            PageHelper.startPage(1, MAX_SIZE_PER_PAGE);
        }
        List<LiveContent> contentList = liveContentMapper.selectByExample(liveContentExample);
        LOG.info("查询记录数为:{}", contentList != null ? contentList.size() : 0);
        return PageInfoUtil.getPageInfoResp(contentList, LiveContent.class);
    }

    /**
     * 保存直播内容
     *
     * @param liveContentSaveReq : 保存实体
     * @author h0ss
     */
    public void save(LiveContentSaveReq liveContentSaveReq) {
        // 创建一个新对象
        LiveContent liveContent = new LiveContent();
        BeanUtils.copyProperties(liveContentSaveReq, liveContent);
        // 判断是新增还是编辑
        if (liveContent.getId() != null) {
            LiveContentExample liveContentExample = new LiveContentExample();
            LiveContentExample.Criteria criteria = liveContentExample.createCriteria();
            criteria.andIdEqualTo(liveContent.getId());
            liveContentMapper.updateByExample(liveContent, liveContentExample);
        } else {
            // 雪花算法生成id
            liveContent.setId(snowFlake.nextId());
            liveContentMapper.insertSelective(liveContent);
        }
        // [可选]直播课程内容修改时向订阅用户发送提醒邮件
    }

    /**
     * 删除直播内容
     *
     * @param id : 要删除的直播内容id
     * @author h0ss
     */
    public void delete(Long id) {
        LiveContentExample liveContentExample = new LiveContentExample();
        LiveContentExample.Criteria criteria = liveContentExample.createCriteria();
        criteria.andIdEqualTo(id);
        liveContentMapper.deleteByExample(liveContentExample);
    }

    /**
     * 根据id获取课时信息
     *
     * @param contentId : 课时id
     * @return : cn.gpnusz.ucloudteach.entity.LiveContent
     * @author h0ss
     */
    public LiveContent getContent(Long contentId) {
        LiveContentExample example = new LiveContentExample();
        LiveContentExample.Criteria criteria = example.createCriteria();
        criteria.andIdEqualTo(contentId);
        List<LiveContent> contentList = liveContentMapper.selectByExample(example);
        if (contentList != null && !contentList.isEmpty()) {
            return contentList.get(0);
        }
        return null;
    }

    /**
     * 开始直播
     *
     * @param userId    : 用户id
     * @param courseId  : 课程id
     * @return : cn.gpnusz.ucloudteach.common.CommonResp<cn.gpnusz.ucloudteach.entity.LiveTokenResp>
     * @author h0ss
     */
    public CommonResp<LiveTokenResp> startLive(Long userId, Long courseId) {
        // 开始获取token
        ApplyTokenReq req = new ApplyTokenReq();
        req.setPublish(Boolean.TRUE);
        // 可能会截断 但现阶段开启直播功能不考虑用户ID的干预，先做保留
        req.setUid(userId.intValue());
        // 作为开启的通道的标识
        String channel = DigestUtils.md5DigestAsHex(courseId.toString().getBytes(StandardCharsets.UTF_8));
        req.setChannel(channel);
        // 重置开始时间
        resetTime(courseId);
        // [可选]开启云端录制功能
        return liveTokenService.getToken(req);
    }

    /**
     * 结束直播
     *
     * @param courseId  : 课程id
     * @param contentId : 课时id
     * @author h0ss
     */
    public void finishLive(Long courseId, Long contentId) {
        // 写直播内容表 修改直播时长参数
        LiveContentExample example = new LiveContentExample();
        LiveContentExample.Criteria criteria = example.createCriteria();
        criteria.andIdEqualTo(contentId);
        criteria.andLiveIdEqualTo(courseId);
        List<LiveContent> contentList = liveContentMapper.selectByExample(example);
        if (contentList == null || contentList.isEmpty()) {
            return;
        }
        LiveContent content = contentList.get(0);
        long startTime = content.getBeginTime().getTime();
        long curTime = System.currentTimeMillis();
        // 写直播时长
        content.setLiveTime(Integer.valueOf(String.valueOf((curTime - startTime) / 1000)));
        // 写直播状态
        content.setStatus(Boolean.TRUE);
        liveContentMapper.updateByExampleSelective(content, example);
        // [可选]结束云端录制功能
    }

    /**
     * 用户参与直播
     *
     * @param userId    : 用户id
     * @param courseId  : 课程id
     * @param contentId : 课时id
     * @return : cn.gpnusz.ucloudteach.common.CommonResp<cn.gpnusz.ucloudteach.entity.LiveTokenResp>
     * @author h0ss
     */
    public CommonResp<LiveTokenResp> joinLive(Long userId, Long courseId, Long contentId) {
        CommonResp<LiveTokenResp> resp = new CommonResp<>();
        // 检查课时开始时间 token获取交给页面初始化去请求
        LiveContentExample example = new LiveContentExample();
        LiveContentExample.Criteria criteria = example.createCriteria();
        criteria.andIdEqualTo(contentId);
        List<LiveContent> contentList = liveContentMapper.selectByExample(example);
        if (contentList == null || contentList.isEmpty()) {
            resp.setSuccess(false);
            resp.setMessage("获取失败，请稍后重试");
            return resp;
        }
        LiveContent liveContent = contentList.get(0);
        // 检查课时开始时间
        long now = System.currentTimeMillis();
        long target = liveContent.getBeginTime().getTime();
        if (target > now) {
            resp.setSuccess(false);
            resp.setMessage("课时仍未开放，请耐心等待");
            return resp;
        }
        // 开始获取token
        ApplyTokenReq req = new ApplyTokenReq();
        req.setPublish(Boolean.FALSE);
        // 可能会截断，但现阶段不考虑用户id的影响，先做保留
        req.setUid(userId.intValue());
        String channel = DigestUtils.md5DigestAsHex(courseId.toString().getBytes(StandardCharsets.UTF_8));
        req.setChannel(channel);
        return liveTokenService.getToken(req);
    }

    /**
     * 用户查询课时状态
     *
     * @param contentId : 课时id
     * @return : cn.gpnusz.ucloudteach.common.CommonResp<java.lang.Boolean>
     * @author h0ss
     */
    public CommonResp<Boolean> checkStatus(Long contentId) {
        CommonResp<Boolean> resp = new CommonResp<>();
        resp.setSuccess(false);
        LiveContentExample example = new LiveContentExample();
        LiveContentExample.Criteria criteria = example.createCriteria();
        criteria.andIdEqualTo(contentId);
        List<LiveContent> contentList = liveContentMapper.selectByExample(example);
        if (contentList != null && !contentList.isEmpty()) {
            resp.setContent(contentList.get(0).getStatus());
            return resp;
        }
        resp.setMessage("状态获取失败");
        return resp;
    }

    /**
     * 重置直播开启时间
     *
     * @param courseId : 直播课程ID
     * @author h0ss
     */
    public void resetTime(Long courseId) {
        LiveContentExample example = new LiveContentExample();
        LiveContentExample.Criteria criteria = example.createCriteria();
        criteria.andLiveIdEqualTo(courseId);
        List<LiveContent> contentList = liveContentMapper.selectByExample(example);
        if (contentList != null && !contentList.isEmpty()) {
            LiveContent liveContent = contentList.get(0);
            Date now = new Date();
            liveContent.setBeginTime(now);
            liveContent.setBeginDate(now);
            liveContentMapper.updateByExample(liveContent, example);
        }
    }
}
