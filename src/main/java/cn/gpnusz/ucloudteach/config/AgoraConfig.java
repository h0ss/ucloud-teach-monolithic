package cn.gpnusz.ucloudteach.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @author h0ss
 * @description 声网直播控制信息
 * @date 2022/4/5 - 15:18
 */
@Configuration
@PropertySource("classpath:agora-application.yml")
@Component
public class AgoraConfig {

    @Value("${cert}")
    public String CERT;

    @Value("${appId}")
    public String APP_ID;

}
