package cn.gpnusz.ucloudteach.controller.live.admin;

import cn.gpnusz.ucloudteach.common.CommonResp;
import cn.gpnusz.ucloudteach.common.PageResp;
import cn.gpnusz.ucloudteach.entity.LiveContent;
import cn.gpnusz.ucloudteach.entity.LiveContentQueryReq;
import cn.gpnusz.ucloudteach.entity.LiveContentSaveReq;
import cn.gpnusz.ucloudteach.service.live.LiveContentServiceImpl;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * @author h0ss
 * @description 直播内容api
 * @date 2022/4/4 - 21:10
 */
@RestController
@RequestMapping("/api/admin/live/content")
public class LiveContentController {

    @Resource
    private LiveContentServiceImpl liveContentService;

    /**
     * 获取直播内容列表
     *
     * @param lcq : 请求实体
     * @return : cn.gpnusz.ucloudteach.common.CommonResp<cn.gpnusz.ucloudteach.common.PageResp<cn.gpnusz.ucloudteach.entity.LiveContent>>
     * @author h0ss
     */
    @GetMapping("/list")
    public CommonResp<PageResp<LiveContent>> list(@Valid LiveContentQueryReq lcq) {
        CommonResp<PageResp<LiveContent>> resp = new CommonResp<>();
        resp.setContent(liveContentService.getAllByCondition(lcq));
        resp.setMessage("获取成功！");
        return resp;
    }

    /**
     * 保存直播
     *
     * @param liveContentSaveReq : 保存实体
     * @return : cn.gpnusz.ucloudteach.common.CommonResp<java.lang.Object>
     * @author h0ss
     */
    @PostMapping("/save")
    public CommonResp<Object> save(@Valid @RequestBody LiveContentSaveReq liveContentSaveReq) {
        liveContentService.save(liveContentSaveReq);
        return new CommonResp<>();
    }

    /**
     * 删除直播
     *
     * @param id : 直播ID
     * @return : cn.gpnusz.ucloudteach.common.CommonResp<java.lang.Object>
     * @author h0ss
     */
    @DeleteMapping("/delete/{id}")
    public CommonResp<Object> delete(@PathVariable Long id) {
        liveContentService.delete(id);
        return new CommonResp<>();
    }
}
