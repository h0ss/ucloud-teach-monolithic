package cn.gpnusz.ucloudteach.controller.live;

import cn.gpnusz.ucloudteach.common.CommonResp;
import cn.gpnusz.ucloudteach.entity.ApplyTokenReq;
import cn.gpnusz.ucloudteach.entity.LiveTokenResp;
import cn.gpnusz.ucloudteach.service.live.LiveTokenServiceImpl;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

/**
 * @author h0ss
 * @description 获取live-token的api
 * @date 2022/4/5 - 17:28
 */
@RestController
public class LiveTokenController {

    @Resource
    private LiveTokenServiceImpl liveTokenService;

    /**
     * 学生申请token
     *
     * @param applyTokenReq : 申请信息
     * @return : cn.gpnusz.ucloudteach.common.CommonResp<cn.gpnusz.ucloudteach.entity.LiveTokenResp>
     * @author h0ss
     */
    @PostMapping("/api/user/live/token/apply")
    public CommonResp<LiveTokenResp> getTokenStu(@RequestBody ApplyTokenReq applyTokenReq) {
        return liveTokenService.stuGetToken(applyTokenReq);
    }

    /**
     * 教师申请token
     *
     * @param applyTokenReq : 申请信息
     * @return : cn.gpnusz.ucloudteach.common.CommonResp<cn.gpnusz.ucloudteach.entity.LiveTokenResp>
     * @author h0ss
     */
    @PostMapping("/api/admin/live/token/apply")
    public CommonResp<LiveTokenResp> getTokenTea(@NotNull @RequestBody ApplyTokenReq applyTokenReq) {
        return liveTokenService.teaGetToken(applyTokenReq);
    }


}
