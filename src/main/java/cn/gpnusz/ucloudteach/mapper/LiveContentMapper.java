package cn.gpnusz.ucloudteach.mapper;

import cn.gpnusz.ucloudteach.entity.LiveContent;
import cn.gpnusz.ucloudteach.entity.LiveContentExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface LiveContentMapper {
    long countByExample(LiveContentExample example);

    int deleteByExample(LiveContentExample example);

    int insert(LiveContent record);

    int insertSelective(LiveContent record);

    List<LiveContent> selectByExample(LiveContentExample example);

    int updateByExampleSelective(@Param("record") LiveContent record, @Param("example") LiveContentExample example);

    int updateByExample(@Param("record") LiveContent record, @Param("example") LiveContentExample example);
}
